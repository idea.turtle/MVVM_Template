####################################################################
#                                注   意                            #
#                         由于使用了删除rm指令请勿修改路径              #
#                         否则可能会误删系统文件引起系统崩溃             #
#                                                                  #
#                                                                  #
####################################################################

#Xcode路径
xcode="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/Library/Xcode/Templates/File Templates/MVVM/"
#文件名
name="MVVM.xctemplate"
#文件完整的路径
path="$xcode$name"
#判断文件是否存在
echo "$path"

if [ ! -d "$xcode" ]; then
echo "Xcode不存在Other目录"
echo "创建Xcode的Other目录"
sudo mkdir "$xcode"
fi

if [ -d "$path" ]; then
echo "$name 文件存在"
#删除文件
sudo rm -rf "$path"
else
echo "$name 不存在"
fi
#复制文件
sudo cp -av "$name" "$path"