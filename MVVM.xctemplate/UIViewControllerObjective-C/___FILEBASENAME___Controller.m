//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#import "___FILEBASENAME___.h"
#import "___FILEBASENAME___ViewModel.h"

@interface ___FILEBASENAMEASIDENTIFIER___ ()

@property (nonatomic, strong) ___FILEBASENAME___ViewModel *viewModel;

@end

@implementation ___FILEBASENAMEASIDENTIFIER___

+ (instancetype)instantiation{
    ___FILEBASENAMEASIDENTIFIER___ * vc = [[___FILEBASENAMEASIDENTIFIER___ alloc] init];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[___FILEBASENAME___ViewModel alloc] init];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
